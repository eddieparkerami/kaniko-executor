#!/bin/sh

echo In entrypoint.sh

echo Testing for git
git --version

echo Testing for git-lfs
git-lfs --version

echo Running kaniko executor with args: $*
/kaniko/executor-actual $*