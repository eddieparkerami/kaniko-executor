## kaniko-executor

A version of kaniko-executor that supports git-lfs and any other utilities I need in the future.

## Description

Default kaniko-executor lacks git-lfs, so I started this project for my own stuff.  Made it public in case others have a similar need.
