FROM gcr.io/kaniko-project/executor:debug-v1.3.0 AS kaniko
FROM alpine/git

RUN apk add --no-cache --update --virtual .build-deps \
		git \
		git-lfs \
	&& git lfs --version

COPY --from=kaniko /kaniko/executor /kaniko/executor-actual

# Wrap the executor with our own script.
COPY src/entrypoint.sh /kaniko/executor
RUN chmod +x /kaniko/executor

ENTRYPOINT ["/kaniko/executor"]